from twisted.internet import protocol,reactor
from twisted.internet.serialport import SerialPort
from twisted.protocols import basic
from twisted.python import log
from datetime import datetime
import sys

class ThermostatControl(basic.LineReceiver):
    def __init__(self):
        self.delimiter="\n"
    def connectionMade(self):
        print "whiii"
    def lineReceived(self,line):
        print line 
        command = "T;{0:%d%m%y}{0:%H%M%S}23\r\n".format(datetime.now())
        print command
        self.sendLine(command)

class thermostatFactory(protocol.ServerFactory):
    protocol = ThermostatControl 


log.startLogging(sys.stdout)
port = SerialPort(ThermostatControl(),"/dev/ttyUSB0",reactor,baudrate="57600")
reactor.run()
