#include <Messenger.h>

#include <Time.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <dht11.h>
#include <stdio.h>


/*define constants*/
#define DHT11PIN 2
#define RELAYPIN 4
#define BATTERYPIN A0
#define BATTERYMAX 969
#define BATTERYMIN  739

/*display status*/
const int DISPLAYTEMP = 1;
const int DISPLAYHUMIDITY = 2;
const int DISPLAYTIME= 3;
const int MEASUREINT = 3000;
const int PHONEHOMEINT = 6000;

/*define variables*/
LiquidCrystal_I2C lcd(0x27,16,2);  
dht11 DHT11;
int displaystatus = DISPLAYTEMP;
int sensorstatus = DHTLIB_OK;
long lastmeasurement=32000;
long lastphonecall = 32000;
int batterystatus = 0;
int target = 02;
Messenger messenger;  


double dewPointFast(double celsius, double humidity)
{
	double a = 17.271;
	double b = 237.7;
	double temp = (a * celsius) / (b + celsius) + log(humidity/100);
	double Td = (b * temp) / (a - temp);
	return Td;
}


void setup()
{
  lcd.init();
  lcd.backlight();
  lcd.print("THERMOSTAT V0.1");
  lcd.setCursor(0,1);
  lcd.print("BOOTING...");
  setTime(00,00,00,01,01,2012);
  pinMode(RELAYPIN,OUTPUT);
  pinMode(13,OUTPUT);
  //delay(1000);
  Serial.begin(57600);
  Serial.setTimeout(500);
  messenger = Messenger(';');
  messenger.attach(messageReady);
}

void loop()
{
  
  if(abs(millis() - lastmeasurement)> MEASUREINT){

    sensorstatus = DHT11.read(DHT11PIN);
    
    lastmeasurement = millis();
    batterystatus=analogRead(BATTERYPIN);
  }
   if(abs(millis() - lastphonecall)> PHONEHOMEINT){
    phoneHome();
    lastphonecall = millis();
  }
  updateDisplay();
  updateRelay();
  
}



void updateRelay(){
  switch (sensorstatus)
  {
    case DHTLIB_OK: 
      if(target > DHT11.temperature){
        digitalWrite(RELAYPIN,LOW);
      }
      else{
	digitalWrite(RELAYPIN,HIGH);
      }
        break;
    default: 
		digitalWrite(RELAYPIN,LOW);
  }
}

void updateDisplay()
{

  switch (sensorstatus)
  {
    case DHTLIB_OK: 
		showInfo();
                break;
    case DHTLIB_ERROR_CHECKSUM: 
		showError();
                break;
    case DHTLIB_ERROR_TIMEOUT: 
		showError();
                break;
    default: 
		showError();
                break;
  }


}

void showError(){
  char firstline[16];
  char secondline[16];
  sprintf(firstline,"ERROR");
  sprintf(secondline,"");
  printToLCD(firstline,secondline);
}
void showInfo()
{
  char firstline[16];
  char secondline[16];
  char colon = ':';
  int batterypercentage = 0;
  if(second()%2==0){
    colon = ' ';
  }
  batterypercentage = map(batterystatus,BATTERYMIN,BATTERYMAX,0,100);
  sprintf(firstline,"%02i%c%02i %02i/%02i %i%%",hour(),colon,minute(),day(),month(),batterypercentage);
  sprintf(secondline,"C:%02i T:%02i H:%02i%%",DHT11.temperature,target,DHT11.humidity);
  //sprintf(secondline,"%i",target);
  printToLCD(firstline,secondline);
}

void printToLCD(char firstline[], char secondline[]){
  char line[17];
  char line2[17];
  lcd.setCursor(0,0);
  sprintf(line,"%-16s",firstline);
  lcd.print(line);
  lcd.setCursor(0,1);
  sprintf(line2,"%-16s",secondline);
  lcd.print(line2);

}

