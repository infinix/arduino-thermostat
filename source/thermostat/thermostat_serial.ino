

void phoneHome()
{

  sendStatus();
  while ( Serial.available() )  messenger.process(Serial.read() );
}

void messageReady(){
  char payload[32];
  char received;
  char data[2];
  digitalWrite(13,HIGH);
  received = messenger.readChar();
  switch(received){
    case 'T':
      messenger.copyString(payload,32);
      setTimeFromSerialData(payload);
      break;
      
  }
  delay(500);
  digitalWrite(13,LOW);
}
//
//void waitForAck(){
//  int received;
//  byte buffer;
//  char payload[32];
// 
//  buffer = readSingleByte();
//                 lcd.setCursor(0,0);
//        lcd.print(buffer);
//        delay(1000);
//  if(buffer!='$')
//  {
//
//    sendError("invallidbeginchar",payload);
//    received= Serial.peek();
//    while(received!=-1 && received !=36){
//      Serial.read();
//      received = Serial.peek();
//    }
//    return;
//  }
//
//  buffer=readSingleByte();
//    switch(buffer){
//      case 'A':
//        if(readPayload(payload) ==-1) return;
//        lcd.setCursor(0,0);
//        lcd.print("ack received");
//        delay(1000);
//        break;
//      default:
//        readPayload(payload);
//        
//        sendError("no ack found",payload);
//        break;
//    }
//}
//
//byte readSingleByte()
//{
//  int counter;
//  int localresult;
//  byte resultbyte;
//  counter=0;
//  while(Serial.available()==0 && counter < 10){
//     counter+=1;
//     delay(100);
//  }
//  if(Serial.available()>0){
//       localresult = Serial.read();
//       resultbyte = (byte)localresult;
//       return resultbyte;
//  }
//  else{
//    
//    sendError("timeout");
//    return '\0';
//  }
//}
//
//int  readPayload(char *payload)
//{
//  int bytesRead=0;
//  bytesRead = Serial.readBytesUntil((char)0x0,payload,32);
//  Serial.read();
//    switch(bytesRead){
//      case 32:
//        sendError("buffer overflow");
//        return -1;
//        break;
//    }
//    return bytesRead;
//}
//
//void acknowledge(){
//  Serial.print("$A");
//  Serial.write("ok");
//  Serial.write((byte)0x0);
//}
//void sendError(char* error){
// Serial.print("$N");
// Serial.print(error);
// Serial.write((byte)0x0); 
// Serial.flush();
//}
//void sendError(char* error,char* error2){
// Serial.print("$N");
// Serial.print(error);
// Serial.print(' ');
// Serial.print(error2);
// Serial.write((byte)0x0); 
// Serial.flush();
//}

void sendStatus()
{
  char statusbuffer[11];
  int batterypercentage = map(batterystatus,BATTERYMIN,BATTERYMAX,0,100);
  sprintf(statusbuffer,"S;%03i%03i%03i\n",DHT11.temperature,DHT11.humidity,batterypercentage);
  Serial.write(statusbuffer);
}


int setTimeFromSerialData(char *buffer)
{
  char data[2];
  data[0]=buffer[0];
  data[1]=buffer[1];
  int day = atoi(data);
  //Serial.write(day);
  data[0]=buffer[2];
  data[1]=buffer[3];
  int month = atoi(data);
  data[0]=buffer[4];
  data[1]=buffer[5];
  int year = atoi(data);
  data[0]=buffer[6];
  data[1]=buffer[7];
  int hour = atoi(data);
  data[0]=buffer[8];
  data[1]=buffer[9];
  int minutes = atoi(data);
  data[0]=buffer[10];
  data[1]=buffer[11];
  int second = atoi(data);
  setTime(hour,minutes,second,day,month,year);
  data[0] = buffer[12];
  data[1] = buffer[13];
  target = atoi(data);
}

